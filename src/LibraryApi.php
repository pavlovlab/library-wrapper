<?php

namespace PavlovLab\Library;

use GuzzleHttp\Client;
use PavlovLab\Library\Contracts\ApiClient;
use PavlovLab\Library\Exceptions\LibraryBadRequestException;

class LibraryApi implements ApiClient
{
    const BASE_API_URL = '94.254.0.188:4000';
    private $client;
    private $availableMethods = ['get'];

    public function __construct()
    {
        $this->client = new Client();
    }

    public function getBooks(array $params): array
    {
        return $this->call('get', '/books', $params);
    }

    public function getAuthors(array $params): array
    {
        return $this->call('get', '/authors', $params);
    }

    public function getBooksForAuthor(int $author_id, array $params): array
    {
        return $this->call('get', "/authors/{$author_id}/books", $params);
    }

    private function fullUrl(string $endpoint): string
    {
        return self::BASE_API_URL . $endpoint;
    }

    private function buildPayload(string $method, array $data): array
    {
        if ($method === 'get')
            return ['query' => $data];
        return $data;
    }

    private function call(string $method, string $endpoint, array $params = []): array
    {
        $method = strtolower($method);
        
        if (!in_array($method, $this->availableMethods)) {
            $message = 'Invalid API call method: ' . $method;
            throw new LibraryBadRequestException($message);
        }

        $url = $this->fullUrl($endpoint);
        $payload = $this->buildPayload($method, $params);

        $response = $this->client->request($method, $url, $payload);
        $body = json_decode($response->getBody(), true);
        
        if (strtolower($body['status']) !== 'ok')
        {
            $this->apiError($body);
        }

        return $body ?? [];
    }

    private function apiError(array $body)
    {
        $message = "Library API error ({$body['status']}): {$body['message']})" ;
        throw new LibraryBadRequestException($message);
    }
}