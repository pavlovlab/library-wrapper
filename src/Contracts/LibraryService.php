<?php

namespace PavlovLab\Library\Contracts;

use \Illuminate\Support\Collection;

interface LibraryService
{
    public function books(array $params): Collection;
    public function authors(array $params): Collection;
    public function authorBooks(int $author_id, array $params): Collection;
}