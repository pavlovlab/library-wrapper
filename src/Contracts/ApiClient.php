<?php

namespace PavlovLab\Library\Contracts;

interface ApiClient
{
    public function getBooks(array $params): array;
	public function getAuthors(array $params): array;
	public function getBooksForAuthor(int $author_id, array $params): array;
}