<?php

namespace PavlovLab\Library;

use Illuminate\Support\ServiceProvider;

class LibraryServiceProvider extends ServiceProvider
{
    /**
     * Register this application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'PavlovLab\Library\Contracts\LibraryService',
            'PavlovLab\Library\Library'
        );

         $this->app->bind(
            'PavlovLab\Library\Contracts\ApiClient',
            'PavlovLab\Library\LibraryApi'
        );
    }
}