<?php

namespace PavlovLab\Library\Models;

use PavlovLab\Library\Models\BaseModel;

class Author extends BaseModel
{
    protected $fillable = ['name', 'id'];
    protected $relations = [
        'book' => 'PavlovLab\\Library\\Models\\Book'
    ];
}