<?php

namespace PavlovLab\Library\Models;

use PavlovLab\Library\Models\BaseModel;

class Book extends BaseModel
{
    protected $fillable = ['title', 'id', 'author'];
    protected $relations = [
        'author' => 'PavlovLab\\Library\\Models\\Author'
    ];
}