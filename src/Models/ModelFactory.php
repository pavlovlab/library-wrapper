<?php

namespace PavlovLab\Library\Models;

class ModelFactory
{
	public static function make(string $modelName, array $data)
	{
		return (new $modelName)->fill($data);
	}
}