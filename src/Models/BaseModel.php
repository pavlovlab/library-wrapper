<?php

namespace PavlovLab\Library\Models;

use PavlovLab\Library\Models\ModelFactory;

class BaseModel
{
    protected $fillable = ['*'];
    protected $relations = [];

    public function fill(array $attr)
    {
        array_walk($attr, function($value, $key) {
            if ($this->isFillable($key)) {
                if (is_array($value) && $this->isRelation($key)) {
                    $value = $this->buildRelation($this->relations[$key], $value);
                }
                $this->{$key} = $value;
            }
        });

        return $this;
    }

    protected function isFillable(string $property): bool
    {
        if ($this->fillable == ['*']) {
            return true;
        }
        
        return in_array($property, $this->fillable);
    }

    protected function isRelation(string $property): bool
    {   
        return array_key_exists($property, $this->relations);
    }

    protected function buildRelation(string $className, array $attributes)
    {   
        return ModelFactory::make($className, $attributes);
    }
}