<?php

namespace PavlovLab\Library;

use PavlovLab\Library\Contracts\LibraryService;
use PavlovLab\Library\Contracts\ApiClient;
use PavlovLab\Library\Models\ModelFactory;
use \Illuminate\Support\Collection;

class Library implements LibraryService
{
    private $api;

    public function __construct(ApiClient $api)
    {
        $this->api = $api;
    }

    public function books(array $params = []): Collection
    {
        $result = $this->api->getBooks($params);
        $books = $result['data']['books'] ?? [];

        return collect($books)->map(function($book) {
            return ModelFactory::make('PavlovLab\\Library\\Models\\Book', $book);
        });
    }

    public function authors(array $params = []): Collection
    {
        $result = $this->api->getAuthors($params);
        $authors = $result['data']['authors'] ?? [];

        return collect($authors)->map(function($author) {
            return ModelFactory::make('PavlovLab\\Library\\Models\\Author', $author);
        });
    }

    public function authorBooks(int $author_id, array $params = []): Collection
    {
        $result = $this->api->getBooksForAuthor($author_id, $params);
        $books = $result['data']['books'] ?? [];
        
        return collect($books)->map(function($book) {
            return ModelFactory::make('PavlovLab\\Library\\Models\\Book', $book);
        });
    }
}